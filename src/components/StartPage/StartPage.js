import './StartPage.css'
import {NavLink} from "react-router-dom";
import {useState} from "react";
import CustomButton from "../CustomButton/CustomButton";

const StartPage = () => {
    const [breed, setBreed] = useState("");

    return (
        <div className={'StartPage'}>
            <div className={'StartPageSearch'}>
                <input type={'text'} placeholder={'Введите породу собаки'} className={'StartPageInput'}
                       onChange={event => setBreed(event.target.value)}
                />
                <NavLink to={`/search/${breed}`}>
                    <div className={'StartPageSearchButton'}>&gt;</div>
                </NavLink>
            </div>
            <br/>
            <NavLink to={"/Voting"}>
                <CustomButton ButtonText={'Выбрать хорошего мальчика'} />
            </NavLink>
            <br/>
            <NavLink to={"/rating"}>
                <CustomButton ButtonText={'Рейтинг хороших мальчиков'} />
            </NavLink>
        </div>
    );
}

export default StartPage;
