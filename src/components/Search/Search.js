import './Search.css'
import BackButton from "../BackButton/BackButton";
import { useParams } from "react-router-dom";
import CustomButton from "../CustomButton/CustomButton";
import Send from "../Send/Send";
import {useEffect, useState} from "react";

const Search = () => {
    const { breed } = useParams()
    const [boy, setBoy] = useState(null)

    useEffect(async () => {
        const answer = await JSON.parse(Send('get', `/find?breedName=${breed}`))
        if(answer && answer.image)
            setBoy(answer)
        else
            alert("Мальчик не найден")
    }, []);

    const handleOnCLick = () => {
        if(boy && boy.image)
            Send('post', `/addToFight`, boy)
    }

    return (
        <div className={'SearchMainContainer'}>
            <BackButton />
            <div className={'SearchImage'} style={{backgroundImage: boy && `url(${boy.image})`}} />
            <br />
            <CustomButton ButtonText={'Добавить к голосованию'} onClick={handleOnCLick}/>
        </div>
    );
}

export default Search;
