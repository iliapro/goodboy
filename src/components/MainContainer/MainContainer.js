import './MainContainer.css'
import { Route, BrowserRouter } from 'react-router-dom';
import StartPage from '../StartPage/StartPage'
import Rating from '../Rating/Rating'
import Search from '../Search/Search'
import Voting from "../Voting/Voting";

const MainContainer = () => {
    return (
        <div className={'mainContainer'}>
            <BrowserRouter>
                <Route exact path={"/"} component={() => <StartPage /> }/>
                <Route exact path={"/rating"} component={() => <Rating /> }/>
                <Route exact path={"/search/:breed"} component={() => <Search /> }/>
                <Route exact path={"/voting"} component={() => <Voting /> }/>
            </BrowserRouter>
        </div>
    );
}

export default MainContainer;
