import './Rating.css'
import BackButton from "../BackButton/BackButton";
import {useEffect, useState} from "react";
import Send from "../Send/Send";

const Rating = () => {
    const [boys, setBoys] = useState(null)

    useEffect(() => {
        const answer = JSON.parse(Send('get', `/rating`))
        answer.sort((a, b) => a.rating > b.rating ? -1 : 1);
        setBoys(answer)
    }, []);

    const createBoys = () => {
        return boys && boys.map(boy => {
            return (<div className={'dogItem'}>
                <div className={'dogImg'}
                     style={{backgroundImage: `url(${boy.image})`}}/>
                <p className={'dogText'}>Голосов: {boy.rating}</p>
            </div>)
        })
    }

    return (
        <div className={'rating'}>
            <BackButton/>
            {createBoys()}
        </div>
    );
}

export default Rating;
