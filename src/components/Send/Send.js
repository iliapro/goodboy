const Send = (method, url, body) => {
    const endPoint = 'https://dog-fighter.herokuapp.com'

    let xhr = new XMLHttpRequest();
    xhr.open(method, endPoint + url, false)
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-type", "application/json")
    xhr.send(JSON.stringify(body))

    xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200)
            return this.responseText
    }

    return xhr.response
}

export default Send;
