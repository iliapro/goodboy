const url = 'localhost:8080'

const {Builder, By} = require('selenium-webdriver')
require('selenium-webdriver/chrome')
require('selenium-webdriver/firefox')
require('chromedriver')
require('geckodriver')

const driver = new Builder().forBrowser('firefox').build()

test('Start page test', async () => {
    await driver.get(url)
    const StartPage = await driver.findElement(By.className('StartPage'))
    const StartPageSearch = await driver.findElement(By.className('StartPageSearch'))
    const StartPageInput = await driver.findElement(By.className('StartPageInput'))
    const StartPageSearchButton = await driver.findElement(By.className('StartPageSearchButton'))

    expect(!!StartPageSearch && !!StartPage && !!StartPageInput && !!StartPageSearchButton).toBe(true)
})

test('Serch page test', async () => {
    await driver.get(`${url}/Search/bulldog`)
    const SearchImage = await driver.findElement(By.className('SearchImage'))
    await new Promise((r) => setTimeout(r, 2500));
    const SearchImageStyle = await SearchImage.getCssValue("background-image")
    expect(SearchImageStyle).not.toBe("none")
}, 20000)

test('Dog dont found test', async () => {
    await driver.get(`${url}/Search/notdog`)
    const SearchImage = await driver.findElement(By.className('SearchImage'))
    const SearchImageStyle = await SearchImage.getCssValue("background-image")
    expect(SearchImageStyle).toBe("none")
}, 20000)

test('Voting page test', async () => {
    await driver.get(`${url}/Voting`)
    const votingButton = await driver.findElement(By.className('votingItemInside'))
    votingButton.click()
})

test('Rating page test', async () => {
    await driver.get(`${url}/Rating`)
    const dogItemsContainer = await driver.findElement(By.className('rating'))
    expect(!!dogItemsContainer).toBe(true)
})

test('Search page back button test', async () => {
    await driver.get(`${url}/Search/bulldog`)
    const backButton = await driver.findElement(By.className('BackButton'))
    backButton.click()
})

test('Voting page back button test', async () => {
    await driver.get(`${url}/Voting`)
    const backButton = await driver.findElement(By.className('BackButton'))
    backButton.click()
})

test('Rating page back button test', async () => {
    await driver.get(`${url}/Rating`)
    const backButton = await driver.findElement(By.className('BackButton'))
    backButton.click()
})
