# Dog fighter

## Instructions:
To install necessary modules: `yarn install`

To bouild project: `yarn build`

To start project: `yarn start`

To test  project: `yarn test`

## Links:
[Online demo](https://sqr-goodboy.herokuapp.com)

[Backend repository](https://gitlab.com/aliendr/sqr-dog-fighter)

[Swagger](https://dog-fighter.herokuapp.com//swagger-ui/#/dog-controller)


##
P.S. Failed to set up tests in CI/CD because one of the team members spent all the allocated pipeline time on building non-working code that didn't even need to be written.
